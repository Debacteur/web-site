function draw_arrow( sel1, locs1, sel2, locs2, arr ) 
{
	var elt1  = sel1;
	var elt2  = sel2;
	var svg = document.querySelector(".roadmap__svg");
	var arrow = document.createElementNS("http://www.w3.org/2000/svg", "path");
        var liste = document.querySelectorAll(".R_step__number")

	var g = document.createElementNS("http://www.w3.org/2000/svg", "g");
	g.classList.add(arr);
	g.setAttribute("fill", "none");
	g.setAttribute("stroke", "#FFCD00");
	g.setAttribute("stroke-width", "15");
	g.prepend(arrow);
	svg.append(g);
	g.style.transform = "translate(-50px, -50px)";
	var posn1 = draw_arrow_pos( elt1, locs1 );
	console.log(liste[0]);
	var posn2 = draw_arrow_pos( elt2, locs2 ); 

	var posn3 = {
		x: (posn2.x - posn1.x)/2,
		y: (posn2.y - posn1.y)/2
	};
//	var d_str =
//		"M" +
//		posn1.x + "," + posn1.y + " " +
//		"L" +
//		posn2.x + "," + posn2.y;
		var d_str =
			"M" +
			posn1.x + "," + posn1.y + " q" + 0 + ", " + posn3.y + " " + posn3.x + ", " + Math.abs(posn3.y ) + " q" + (posn3.x) + ",0 " + (posn3.x) + ", " + (posn3.y);
	arrow.setAttribute( "d", d_str );
}


/* loc is a two-character string whose 
   first character is one of "l", "m" or "r",
   and whose second is one of "l", "m" or "r".
   elt is a DOM element.
   Returns a point with selectors 'x'
   and 'y', being the x and y coordinates
   of the (left, middle, or right) 
   and (top, middle, or bottom) of elt.
   */
function draw_arrow_pos( elt, locs )
{
	return(    
		{ x: draw_arrow_x_pos( elt, locs.substring(0,1) ),
			y: draw_arrow_y_pos( elt, locs.substring(1,2) )
		}
	);
}


/* loc is one of "l", "m" or "r". elt is
   a DOM element. Returns the x coordinate
   of the left, middle, or right of elt.
   */
function draw_arrow_x_pos( elt, loc )
{
	return(     
		elt.offsetLeft +
		( loc == "l" ? 0 :
			( loc == "m" ? elt.offsetWidth / 2 :
				/*"r"*/ elt.offsetWidth 
			))
	); 
}


/* loc is one of "t", "m" or "b". elt is
   a DOM element. Returns the y coordinate
   of the top, middle, or bottom of elt.
   */
function draw_arrow_y_pos( elt, loc )
{
	return(
		elt.offsetTop +
		( loc == "t" ? 0 :
			( loc == "m" ? elt.offsetHeight / 2 :
				/*"b"*/ elt.offsetHeight 
			))
	); 

}

function draw_arrows(liste) {
	
draw_arrow( liste[0], "mm", liste[1] , "mt", "arrow1" ); 
draw_arrow( liste[1], "mm", liste[2], "mt", "arrow2" ); 
draw_arrow( liste[2], "mm", liste[3], "mt", "arrow3" ); 
}

draw_arrows(document.querySelectorAll(".R_step__number"));
