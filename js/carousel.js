/* ====================================
 *
 * 		CAROUSEL
 *
 * ================================= */

class Carousel {
	constructor(el, options = {}) {
		const DEFAULTS = {
			infinite: true,
			slideSelector:  ".Carousel__slide",
			initialSlideIndex: 0
		};
		this.carousel = el;
		this.settings = {
			...DEFAULTS,
			...options
		};

		this.slides = el.querySelectorAll(this.settings.slideSelector);

		this.state = {
			currentSlide: this.settings.initialSlideIndex
		};

		this.track = null;
		this.navigation = null;
		this.navigationButtons = [];

		// Init
		this.init();
	}

	setState(newState, callback = () => undefined) {
		this.state = {
			...this.state,
			...newState
		};
		this.updateCarousel();
		return callback();
	}

	init() {
		const { carousel, slides } = this;
		this.onInitActions();

		window.addEventListener("resize", () => {
			setTimeout(() => {
				this.onResizeActions();
			}, 100);
		});
	}

	onInitActions() {
		this.createSlideTrack();
		this.createNavigation();
		this.createNavigationButtons();
		this.updateCarousel();
		this.carousel.classList.add("initialized");
	}

	onResizeActions() {
		this.setSlideTrackDimensions();
	}

	createSlideTrack() {
		const { slides, carousel, track } = this;

		// Do not re-create track if it exists
		if (!track) {
			const track = document.createElement("div");
			track.classList.add("C-track");
			slides.forEach((slide) => {
				track.appendChild(slide);
			});
			if (carousel == document.querySelectorAll(".testimonials-carousel__wrapper")[0].parentElement) {
				document.querySelectorAll(".testimonials-carousel__wrapper")[0].appendChild(track); 
			} else {
			carousel.appendChild(track);
			}
			this.track = track;
		}
		this.setSlideTrackDimensions();
	}

	setSlideTrackDimensions() {
		const { track, slides } = this;
		slides.forEach((slide) => {
			slide.style.transition = "none";
		});
		const numberOfSlides = slides.length;

		if (!track) {
			return;
		}

		const height = [...slides].reduce(
			(acc, slide) => acc + slide.offsetHeight,
			0
		);
		const width = [...slides].reduce(
			(acc, slide) => acc + slide.offsetWidth,
			0
		);

		track.style.transition = "none";

		track.style.width = width + "px";
		track.style.height = height / numberOfSlides + "px";

		track.style.transition = "";
		slides.forEach((slide) => {
			slide.style.transition = "";
		});
	}

	updateTrackPosition() {
		const { slides, track } = this;

		const numberOfSlides = slides.length;
		const basePercentage = 100 / numberOfSlides;

		const { currentSlide } = this.state;

		track.style.transform = `translateX(-${basePercentage * currentSlide}%)`;
	}

	createNavigation() {
		const { slides, carousel, navigation } = this;
		if (!navigation) {
			const navigationContainer = document.createElement("div");
			navigationContainer.classList.add("C-navigation");
			[...slides].forEach((slide, index) => {
				navigationContainer.appendChild(this.createNavigationDot(index));
			});
			carousel.appendChild(navigationContainer);
			this.navigation = navigationContainer;
		}
	}

	createNavigationDot(index) {
		const { currentSlide } = this.state;
		const navigationDot = document.createElement("div");
		navigationDot.classList.add("C-navigation__dot");
		navigationDot.setAttribute("data-slideIndex", index);
		navigationDot.addEventListener("click", () => {
			this.goTo(index);
		});

		return navigationDot;
	}

	createNavigationButtons() {
		const createNavigationButton = (buttonType) => {
			const { carousel } = this;
			const navigationButton = document.createElement("button");
			navigationButton.setAttribute("type", "button");
			navigationButton.classList.add("Carousel-navigation-button");
			navigationButton.classList.add(
				`Carousel-navigation-button--${buttonType}`
			);
			navigationButton.addEventListener("click", () => {
				const { slides } = this;
				const { currentSlide } = this.state;
				const numberOfSLides = slides.length;
				const lastSlideIndex = numberOfSLides - 1;

				if (buttonType === "next") {
					const slideToGo = currentSlide + 1;
					if (slideToGo > lastSlideIndex) {
						if (this.settings.infinite) {
							return this.goTo(0);
						}
						return;
					} else {
						this.goTo(slideToGo);
					}
				} else {
					const slideToGo = currentSlide - 1;
					if (slideToGo < 0) {
						if (!!this.settings.infinite) {
							return this.goTo(lastSlideIndex);
						}
						return;
					} else {
						this.goTo(slideToGo);
					}
				}
			});

			// Change here the text inside the button
			if (buttonType === "next") {
				navigationButton.textContent = ">";
			} else {
				navigationButton.textContent = "<";
			}
			carousel.appendChild(navigationButton);
			this.navigationButtons.push(navigationButton);
		};

		if (this.navigationButtons.length <= 0) {
			createNavigationButton("next");
			createNavigationButton("prev");
		}
	}

	updateNavigation() {
		const { navigation } = this;

		const { currentSlide } = this.state;

		const navigationDots = navigation.querySelectorAll(".C-navigation__dot");
		navigationDots.forEach((dot) => {
			const dotIndex = parseInt(dot.getAttribute("data-slideIndex"), 10);
			if (dotIndex === currentSlide) {
				dot.classList.add("active");
			} else {
				dot.classList.remove("active");
			}
		});
	}

	updateSlides() {
		const { slides } = this;

		const { currentSlide } = this.state;

		slides.forEach((slide, index) => {
			if (index === currentSlide) {
				slide.classList.add("active");
			} else {
				slide.classList.remove("active");
			}
		});
	}

	goTo(index){
		this.setState(
			{
				currentSlide: index
			},
			() => {
				//console.log('Current slide is now: ', this.state.currentSlide);
			}
		);
	};

	updateCarousel() {
		this.updateTrackPosition();
		this.updateNavigation();
		this.updateSlides();
	}
}

if(document.querySelector(".Carousel")) {
	const carousel = new Carousel(document.querySelectorAll(".Carousel")[0]);
	if(document.querySelectorAll(".Carousel").length > 1) {
		const testimonials_carousel = new Carousel(document.querySelectorAll(".Carousel")[1]);
	}
}


class MiniCarousel {
	constructor(miniCarousel, options = {}) {
		const DEFAULTS = {
			initialSlideIndex: 0,
			miniSlidesSelector: ".workshop-presentation"
		};
		this.miniCarousel = miniCarousel;
		this.settings = {
			...DEFAULTS,
			...options
		};
		this.miniSlides = document.querySelectorAll(this.settings.miniSlidesSelector);
		this.track = null;
		this.state = {
			currentSlide: this.settings.initialSlideIndex
		}
		this.navigationButtons = [];

		this.init();
	}

	init() {
		const { miniCarousel, miniSlides } = this;

		this.onInitActions();

		window.addEventListener("resize", () => {
			setTimeout(() => {
				this.onResizeActions();
			}, 100);
		});
	}

	setState(newState, callback = () => undefined) {
		this.state = {
			...this.state,
			...newState
		};
		this.updateCarousel();
		return callback();
	}
	onInitActions() {
		this.createNavigationButtons();
		this.createSlideTrack();
		this.createNavigation();
		this.updateCarousel();
	}

	createNavigationButtons() {
		const createNavigationButton = (buttonType) => {
			const { miniCarousel } = this;
			const navigationButton = document.createElement("button");
			navigationButton.setAttribute("type", "button");
			navigationButton.classList.add("Carousel-navigation-button");
			navigationButton.classList.add(
				`Carousel-navigation-button--${buttonType}`
			);
			navigationButton.addEventListener("click", () => {
				const { miniSlides } = this;
				const { currentSlide } = this.state;
				const numberOfSLides = miniSlides.length;
				const lastSlideIndex = numberOfSLides - 1;

				if (buttonType === "next") {
					const slideToGo = currentSlide + 1;
					if (slideToGo > lastSlideIndex) {
						if (this.settings.infinite) {
							return this.goTo(0);
						}
						return;
					} else {
						this.goTo(slideToGo);
					}
				} else {
					const slideToGo = currentSlide - 1;
					if (slideToGo < 0) {
						if (!!this.settings.infinite) {
							return this.goTo(lastSlideIndex);
						}
						return;
					} else {
						this.goTo(slideToGo);
					}
				}
			});

			// Change here the text inside the button
			if (buttonType === "next") {
				navigationButton.textContent = ">";
			} else {
				navigationButton.textContent = "<";
			}
			miniCarousel.appendChild(navigationButton);
			this.navigationButtons.push(navigationButton);
		};

		if (this.navigationButtons.length <= 0) {
			createNavigationButton("next");
			createNavigationButton("prev");
		}
	}
	updateCarousel() {
		this.updateTrackPosition();
		this.updateNavigation();
	}
	createNavigation() {
		const { miniSlides, miniCarousel, navigation } = this;
		if (!navigation) {
			const navigationContainer = document.createElement("div");
			navigationContainer.classList.add("C-navigation");
			[...miniSlides].forEach((slide, index) => {
				navigationContainer.appendChild(this.createNavigationDot(index));
			});
			miniCarousel.appendChild(navigationContainer);
			this.navigation = navigationContainer;
		}
	}
	createSlideTrack() {
		const { miniSlides, miniCarousel, track } = this;

		// Do not re-create track if it exists
		if (!track) {
			const track = document.createElement("div");
			track.classList.add("C-track");
			miniSlides.forEach((slide) => {
				track.appendChild(slide);
			});

			miniCarousel.appendChild(track);
			this.track = track;
		}
		this.setSlideTrackDimensions();
	}

	setSlideTrackDimensions() {
		const { track, miniSlides } = this;
		miniSlides.forEach((slide) => {
			slide.style.transition = "none";
		});
		const numberOfminiSlides = miniSlides.length;

		if (!track) {
			return;
		}

		const height = [...miniSlides].reduce(
			(acc, slide) => acc + slide.offsetHeight,
			0
		);
		const width = [...miniSlides].reduce(
			(acc, slide) => acc + slide.offsetWidth,
			0
		);

		track.style.transition = "none";
		if (window.innerWidth >= 1068) {
			track.style.width = "1068px";
		} else if(window.innerWidth >= 480) {
			track.style.width = window.innerWidth - 10 + "px";
		} else {
			track.style.width = width + 10 + "px";
		}
		track.style.height = height / numberOfminiSlides + "px";

		track.style.transition = "";
		miniSlides.forEach((slide) => {
			slide.style.transition = "";
		});
	}

	createNavigationDot(index) {
		const { currentSlide } = this.state;
		const navigationDot = document.createElement("div");
		navigationDot.classList.add("C-navigation__dot");
		navigationDot.setAttribute("data-slideIndex", index);
		navigationDot.addEventListener("click", () => {
			this.goTo(index);
		});

		return navigationDot;
	}
	goTo(index){
		this.setState(
			{
				currentSlide: index
			},
			() => {
				//console.log('Current slide is now: ', this.state.currentSlide);
			}
		);
	};
	updateTrackPosition() {
		const { miniSlides, track } = this;

		const numberOfminiSlides = miniSlides.length;
		const basePercentage = 100 / numberOfminiSlides;

		const { currentSlide } = this.state;

		track.style.transform = `translateX(-${basePercentage * currentSlide}%)`;
	}

	updateNavigation() {
		const { navigation } = this;

		const { currentSlide } = this.state;

		const navigationDots = navigation.querySelectorAll(".C-navigation__dot");
		navigationDots.forEach((dot) => {
			const dotIndex = parseInt(dot.getAttribute("data-slideIndex"), 10);
			if (dotIndex === currentSlide) {
				dot.classList.add("active");
			} else {
				dot.classList.remove("active");
			}
		});
	}
	onResizeActions() {
		this.setSlideTrackDimensions();
	}
}
	if(document.querySelector(".Carousel-workshop")) {
		const workshop_carousel = new MiniCarousel(document.querySelectorAll(".Carousel-workshop")[0]);
	}
