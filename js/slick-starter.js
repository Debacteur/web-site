$('.carousel').slick({
	infinite: true,
	slidesToShow: 3, // Shows a three slides at a time
	slidesToScroll: 3, // When you click an arrow, it scrolls 1 slide at a time
	arrows: true, // Adds arrows to sides of slider
	dots: true, // Adds the dots on the bottom
	autoplay: true,
	autoplaySpeed: 3500,
	responsive: [
		{
			breakpoint: 580,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		},
		{
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		}

	],
});
