var wrapper = document.querySelectorAll(".wrapper")[0];
var sidebar = document.querySelectorAll(".menu-nav--side-bar")[0];
var hamburger = document.querySelectorAll(".menu-nav__item--right")[0];
const header = document.querySelectorAll(".header")[0];
const menuNav = document.querySelectorAll(".menu-nav")[0];

hamburger.addEventListener("click", () => {
	if (!document.querySelectorAll(".overlay-mobile")[0])
	{
		const overlay = document.createElement("div");
		sidebar.classList.add("active");
		overlay.classList.add("overlay-mobile");
		wrapper.appendChild(overlay);
		wrapper.appendChild(sidebar);
	}
	else {
		const overlay = document.querySelectorAll(".overlay-mobile")[0];
		overlay.classList.remove("overlay-mobile");
		sidebar.classList.remove("active");
	}
});

const overlay = document.querySelectorAll(".overlay-mobile")[0];
const titles = document.querySelectorAll(".title");
const mobileHomepage = document.querySelectorAll(".menu-nav__item--mobile")[0];
const sidebarBottom = document.querySelectorAll(".side-bar__bottom")[0];

function placeLogo() {
	const logo = document.querySelectorAll(".header__logo")[0];
		
	if(window.innerWidth <= 480) {
		logo.style.display = "block";
		wrapper.prepend(logo);
		if (window.innerWidth <= 360) {
			mobileHomepage.firstChild.nextSibling.childNodes[1].style.display = "none";	
			mobileHomepage.firstChild.nextSibling.childNodes[3].style.display = "inline-block";
			sidebarBottom.prepend(mobileHomepage);
		} else {
			mobileHomepage.firstChild.nextSibling.childNodes[1].style.display = "block";	
			mobileHomepage.firstChild.nextSibling.childNodes[3].style.display = "none";
			menuNav.prepend(mobileHomepage);
		}
	} else {
		logo.style.display = "block";
		header.prepend(logo);
	}
}

function showtitles(titles) {
	titles.forEach((element, index) => {
		element.style.width = "";
		if (element.clientWidth >= window.innerWidth) {
			if (element.firstChild.data == "Ils nous font confiance") {
				element.style.width = "184px";
			}
		} 
	});
}
showtitles(titles);
placeLogo();

window.addEventListener("resize", () => {
	setTimeout(() => {
		placeLogo();
		showtitles(titles);
	}, 100);
});

if(window.innerWidth <= 480 && document.querySelector('.debacteur-workshop')) {
	const workshops = document.querySelectorAll('.debacteur-workshop');
	const slides = document.querySelectorAll('.workshop-presentation');
	
	function colapse_workshop(workshop, slide, button) {
		button.parentNode.removeChild(button);
		workshop.style.display = 'none';
		slide.childNodes[7].style.display = 'block';
		slide.parentNode.parentNode.childNodes[4].style.display = 'block';
		slide.parentNode.parentNode.childNodes[5].style.display = 'block';
		slide.parentNode.parentNode.childNodes[7].style.display = 'block';
	}
	function display_workshop(workshop, slide) {
		workshop.style.display = 'block';
		var button = document.createElement('a');
		button.innerText = 'Découvrir un autre atelier';
		button.classList.add('colapse-workshop');
		button.setAttribute('href', '#carousel');
		wrapper.append(button);
		slide.parentNode.parentNode.childNodes[4].style.display = 'none';
		slide.parentNode.parentNode.childNodes[5].style.display = 'none';
		slide.parentNode.parentNode.childNodes[7].style.display = 'none';
		
		button.addEventListener('click', function() {
			colapse_workshop(workshop, slide, this);
		}, false);
	}


	for(i = 0; i <slides.length; i++) {
		(function () {
			var workshop = workshops[i];
			var slide = slides[i];
			workshop.style.display = 'none';
			workshop.style.width = 'unset';
			slide.append(workshop);

			slide.childNodes[7].addEventListener('click', function() {
				this.style.display = 'none';
				display_workshop(workshop, slide);}, false);
		}());
			
	}
}
