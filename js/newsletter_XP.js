
$().ready(function() {
	if((screen.width >= 640)) {
		$(".newsletter").on("click", "div.CTA", function() {
			var _this = this;
			$(this).css("width", "600px");
			setTimeout(function() {
				$(_this).css("opacity", 0);
				setTimeout(function() {
					$("div.disolve")
						.show()
						.css("opacity", 1);
				}, 300);
			}, 0);
		});
		$(".newsletter").on("submit", "form", function() {
			$("input, #submit").css("opacity", 0);
			setTimeout(function() {
				$("input, #submit").hide();
				$("p").show();
				setTimeout(function() {
					$("p").css("opacity", 1);
				}, 1);
			}, 300);
		});
	} else {
		$(".newsletter").on("click", "div.CTA", function() {
			var _this = this;
			$(this).css("width", "300px");
			setTimeout(function() {
				$(_this).css("opacity", 0);
				setTimeout(function() {
					$("div.disolve")
						.show()
						.css("opacity", 1);
				}, 300);
			}, 0);
		});
		$(".newsletter").on("submit", "form", function() {
			$("input, #submit").css("opacity", 0);
			setTimeout(function() {
				$("input, #submit").hide();
				$("p").show();
				setTimeout(function() {
					$("p").css("opacity", 1);
				}, 1);
			}, 300);
		});
	}
});
