# DÉB'ACTEUR - WEBSITE

Déb'acteur organises social and political debates to make citizens rise their voices.

Déb'acteur needed a website that expresses with colors, texts and images the playful and learning experiences it proposes.

## Technical stack

1. HTML
1. SASS
1. PLAIN JAVASCRIPT
1. JEKYLL as static website generator
